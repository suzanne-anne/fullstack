console.log(document.querySelector("#txt-first-name"))


const txt_first_name = document.querySelector("#txt-first-name")
const txt_last_name = document.querySelector("#txt-last-name")
let span_full_name = document.querySelector("#txt-last-name")

// [Activity Part]
  let updateFullNameSpan = () => {
      let firstNameInput = document.getElementById('firstName');
      let lastNameInput = document.getElementById('lastName');
      let fullNameHeading = document.getElementById('fullName');

      let fullName = firstNameInput.value + ' ' + lastNameInput.value;
      fullNameHeading.textContent = fullName;
    }

    let firstNameInput = document.getElementById('firstName');
    let lastNameInput = document.getElementById('lastName');

    firstNameInput.addEventListener('input', updateFullNameSpan);
    lastNameInput.addEventListener('input', updateFullNameSpan);